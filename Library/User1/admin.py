from django.contrib import admin

from .models import Author,BookStatus, Book, BookInstance

# Register your models here.

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(BookInstance)
admin.site.register(BookStatus)

