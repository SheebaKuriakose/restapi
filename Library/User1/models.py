from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.auth.models import AbstractUser

class Author(models.Model):
    author = models.CharField(max_length=100)

    def __str__(self):
        return self.author

class BookStatus(models.Model):
    status = models.CharField(max_length=100)

    def __str__(self):
        return self.status

class Book(models.Model):

    class BookObjects(models.Manager):
        def get_queryset(self):
            return super().get_queryset().filter(Available=1)

    author = models.ForeignKey(Author, on_delete=models.PROTECT, default=1)
    title = models.CharField(max_length=250)
    published = models.DateTimeField(default=timezone.now)
    owner = models.ForeignKey('auth.user', related_name='posts', on_delete=models.CASCADE)
    Available = models.ForeignKey('BookStatus', on_delete=models.SET_NULL, null=True)

    objects = models.Manager()
    bookobjects = BookObjects()

    class Meta:
        ordering = ('-Available',)

    def __str__(self):
        return self.title 


class BookInstance(models.Model):
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True) 
    owner = models.ForeignKey('auth.user', related_name='bookposts', on_delete=models.CASCADE)
    due_back = models.DateField(null=True, blank=True)
    status = models.ForeignKey('BookStatus', on_delete=models.SET_NULL, null=True)
    #bookstatus = models.OneToOneField("User.Book", verbose_name=("Available"), on_delete=models)

    #objects = models.Manager()
bookinstance = BookInstance()


