from rest_framework import serializers
from .models import Book, Author, BookInstance
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    posts = serializers.PrimaryKeyRelatedField(many =True, queryset=Book.objects.all())
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = User
        fields = ('id', 'username', 'posts', 'owner')

    

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'],  validated_data['password'])

        return user

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'author', 'published', 'Available')

    def create(self, validated_data):
        return Book.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.excerpt = validated_data.get('author', instance.excerpt)
        instance.content = validated_data.get('published', instance.content)
        instance.status = validated_data.get('Available', instance.status)
        instance.save()
        return instance

class BookSerializerUnauthorized(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'author', 'Available')

class BookInstanceSerializer(serializers.ModelSerializer):
    #bookposts = serializers.PrimaryKeyRelatedField(many =True, queryset=BookInstance.objects.all())
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = BookInstance
        fields = ['id', 'due_back', 'book', 'owner', 'status']

class BookInstanceEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookInstance
        fields = ['book', 'status']

