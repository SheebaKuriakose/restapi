from django.urls import path, include
from User1.views import BookDetail, BookList, RegisterAPI, UserList, UserDetails, BookInstanceDetail, BookInstanceEditDetails
from .import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('<int:pk>/', views.BookDetail.as_view()),
    path('', views.BookList.as_view()),
    path('register', views.RegisterAPI.as_view()),
    path('user/<int:pk>/', views.UserDetails.as_view()),
    path('user', views.UserList.as_view()),
    path('instance/', views.BookInstanceDetail.as_view()),
    path('instance/<int:pk>/', views.BookInstanceEditDetails.as_view()),
    #path('', views.)
    

    #path('', views.BookListUnauthorized.as_view()),
    
]

