from django.shortcuts import render
from rest_framework.response import Response
from .models import Book, Author, BookInstance
from .serializers import BookSerializer, BookSerializerUnauthorized, RegisterSerializer, BookInstanceSerializer, BookInstanceEditSerializer

from rest_framework import status
from rest_framework.views import APIView
from django.http import Http404
from rest_framework import mixins
from rest_framework import generics
from django.contrib.auth.models import User
from .serializers import UserSerializer
from rest_framework import permissions
from .permissions import IsOwnerOrReadOnly
from .permissions import IsReadOnly

from knox.models import AuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetails(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        return Response({
        "user": UserSerializer(user, context=self.get_serializer_context()).data,
        "token": AuthToken.objects.create(user)[1]
        })

class BookList(generics.ListCreateAPIView):
    queryset = Book.bookobjects.all()
    serializer_class = BookSerializerUnauthorized
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)       
    
class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user) 

class BookInstanceDetail(generics.ListCreateAPIView):
    queryset = BookInstance.objects.all()
        
    l = len(queryset)
    if l<2:
        print("hello")
        serializer_class = BookInstanceSerializer 
        permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]               
    else:
        print("Limit exceeded")   
        serializer_class = BookInstanceSerializer          
        permission_classes = [IsReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user) 

"""
    print(l)
    if l<2:
        serializer_class = BookInstanceSerializer      
    else:
        print("Limit exceeded")
    serializer_class = BookInstanceSerializer 
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user) 
"""
class BookInstanceEditDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = BookInstance.objects.all()
    serializer_class = BookInstanceEditSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user) 

    
    

"""
class BookListUnauthorized(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializerUnauthorized
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user) 
"""